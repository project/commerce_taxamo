Taxamo for Commerce
===================

Dependencies
------------

- [Commerce](http://drupal.org/project/commerce)
- [Libraries API 2.x](http://drupal.org/project/libraries)
- [Taxamo PHP](https://github.com/taxamo/taxamo-php)

Documentation
-------------

Please refer to the documentation page (https://www.drupal.org/node/2411879) for
Installation and Configuration process.
